#include "roku/RokuChannel.hpp"

namespace Roku
{
  RokuChannel::RokuChannel(std::string name,
                           std::map<std::string, std::string> attributes,
                           std::string iconBytes)
  {
    this->name = name;
    this->attributes = attributes;
    this->iconBytes = iconBytes;
  }

  RokuChannel::RokuChannel() {}

  std::string RokuChannel::getName() const { return this->name; }

  std::string RokuChannel::getAttribute(std::string attributeName) const
  {
    std::map<std::string, std::string>::const_iterator iter =
        this->attributes.find(attributeName);

    if (iter != this->attributes.end())
    {
      return iter->second;
    }

    return "";
  }

  std::string RokuChannel::getIconBytes() const { return this->iconBytes; }
}  // namespace Roku
