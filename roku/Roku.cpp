#include "roku/Roku.hpp"

#include <cpr/cpr.h>
#include <tinyxml2.h>

#include <fstream>
#include <iostream>

#include "cpr/api.h"
#include "cpr/cprtypes.h"
#include "cpr/status_codes.h"

namespace Roku
{
  Roku::Roku(std::string ip, std::string port)
  {
    this->ip = ip;
    this->port = port;
    this->baseUrl = "http://" + this->ip + ":" + this->port;
    this->downloadChannels();
  }

  std::vector<RokuChannel> Roku::getChannels() const { return this->channels; }

  bool Roku::hasChannel(std::string name) const
  {
    unsigned int size = this->channels.size();
    for (unsigned int k = 0; k < size; k++)
    {
      if (this->channels[k].getName() == name)
      {
        return true;
      }
    }

    return false;
  }

  std::string Roku::getIp() const { return this->ip; }

  bool Roku::launch(std::string name) const
  {
    for (const RokuChannel& channel : this->channels)
    {
      if (channel.getName() == name)
      {
        cpr::Url url(this->launchUrl(channel.getAttribute("id")));
        cpr::Response rep = cpr::Post(url, cpr::Payload{{}});
        return rep.status_code == cpr::status::HTTP_NO_CONTENT;
      }
    }
    return false;
  }

  void Roku::downloadChannels()
  {
    cpr::Url url(this->queryUrl("apps"));
    cpr::Response rep = cpr::Get(url);
    if (rep.status_code != cpr::status::HTTP_OK)
    {
      return;
    }
    tinyxml2::XMLDocument doc;
    doc.Parse(rep.text.c_str());

    tinyxml2::XMLElement* apps = doc.FirstChildElement("apps");
    tinyxml2::XMLElement* app = apps->FirstChildElement();
    while (app != nullptr)
    {
      std::string title = std::string(app->GetText());
      std::map<std::string, std::string> attributes;

      const tinyxml2::XMLAttribute* attr = app->FirstAttribute();
      while (attr != nullptr)
      {
        attributes[attr->Name()] = attr->Value();
        attr = attr->Next();
      }

      std::string id = std::string(app->Attribute("id"));
      cpr::Url iconUrl("http://" + this->ip + ":" + this->port +
                       "/query/icon/" + id);
      cpr::Response iconRep = cpr::Get(iconUrl);
      std::string icon = iconRep.text;

      RokuChannel channel(title, attributes, icon);
      this->channels.push_back(channel);

      app = app->NextSiblingElement();
    }

    this->haveChannelList = this->channels.size() > 0;
  }

  std::string Roku::getPort() const { return this->port; }

  bool Roku::hasChannelList() const { return this->haveChannelList; }

  std::string Roku::queryUrl(std::string q) const
  {
    return this->baseUrl + "/query/" + q;
  }

  std::string Roku::keypressUrl(std::string k) const
  {
    return this->baseUrl + "/keypress/" + k;
  }

  std::string Roku::launchUrl(std::string c) const
  {
    return this->baseUrl + "/launch/" + c;
  }

  std::future<cpr::Response> Roku::sendKey(Key key) const
  {
    cpr::Url url(this->keypressUrl(key.toString()));
    return cpr::PostAsync(url);
  }

  std::future<cpr::Response> Roku::sendChar(std::string c) const
  {
    cpr::Url url(this->keypressUrl("Lit_" + c));
    return cpr::PostAsync(url);
  }
}  // namespace Roku
