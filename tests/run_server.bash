#!/usr/bin/env bash

cmd=${1}
if [[ ${cmd} == "start" ]]
then
		export FLASK_APP=test_server.py
		flask run 2>&1 >/dev/null &

		while ! curl -s http://0.0.0.0:5000/query/apps 2>&1 >/dev/null
		do
				sleep 1s
		done
		printf 'Server started'
elif [[ ${cmd} == "stop" ]]
then
		curl http://0.0.0.0:5000/shutdown
else
		printf 'CMD must be one of {start,stop}\n'
fi
