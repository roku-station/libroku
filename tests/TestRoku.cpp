#include <roku/Roku.hpp>

#include "cpr/status_codes.h"
#define CATCH_CONFIG_MAIN

#include <cpr/cpr.h>
#include <cpr/response.h>

#include <future>

#include "catch2/catch.hpp"
#include "roku/Roku.hpp"

TEST_CASE("Roku objects should populate correctly")
{
  Roku::Roku roku("0.0.0.0", "5000");

  SECTION("All channels are downloaded")
  {
    REQUIRE(roku.hasChannelList());
    std::vector<Roku::RokuChannel> channels = roku.getChannels();
    REQUIRE(channels.size() == 1);
    REQUIRE(channels[0].getName() == "App 0");
    REQUIRE(roku.hasChannel("App 0"));
    REQUIRE_FALSE(roku.hasChannel("Foobar"));
    REQUIRE(channels[0].getAttribute("id") == "000");
    REQUIRE(channels[0].getAttribute("foobar") == "");
    REQUIRE(channels[0].getIconBytes() != "");
  }

  SECTION("Object attributes are set")
  {
    REQUIRE(roku.getIp() == "0.0.0.0");
    REQUIRE(roku.getPort() == "5000");
  }
}

TEST_CASE("Roku objects should send keypresses")
{
  Roku::Roku roku("0.0.0.0", "5000");

  auto key = GENERATE(Roku::Key::Up, Roku::Key::Down, Roku::Key::Left,
                      Roku::Key::Right, Roku::Key::Backspace, Roku::Key::Select,
                      Roku::Key::Back, Roku::Key::Space, Roku::Key::PlayPause,
                      Roku::Key::Reverse, Roku::Key::Forward, Roku::Key::Home);

  SECTION(Roku::Key(key).toString())
  {
    std::future<cpr::Response> result = roku.sendKey(key);
    result.wait();

    REQUIRE(result.get().status_code == cpr::status::HTTP_NO_CONTENT);
  }

  auto c = GENERATE("a", "b", "!", " ");
  SECTION(c)
  {
    std::future<cpr::Response> result = roku.sendChar(c);
    result.wait();

    REQUIRE(result.get().status_code == cpr::status::HTTP_NO_CONTENT);
  }
}

TEST_CASE("Objects should correctly send launch commands")
{
  Roku::Roku roku("0.0.0.0", "5000");

  SECTION("Launch test channel") { REQUIRE(roku.launch("App 0")); }

  SECTION("Launch nonexistent channel")
  {
    REQUIRE_FALSE(roku.launch("Foobar"));
  }
}
