from flask import Flask, Response, send_from_directory, send_file, request

app = Flask(__name__, static_url_path="")

def shutdown_server():
    func = request.environ.get("werkzeug.server.shutdown")
    if func is not None:
        func()

@app.route("/query/apps")
def apps():
    return send_file("data/test_app_list.xml")

@app.route("/query/icon/<name>")
def icon(name):
    return send_file(f"data/{name}.png")

@app.route("/keypress/<key>", methods=["POST"])
def keypress(key):
    return Response(""), 204

@app.route("/launch/<channel>", methods=["POST"])
def launch(channel):
    return Response(""), 204

@app.route("/shutdown")
def shutdown():
    shutdown_server()
    return "Shutting down"
