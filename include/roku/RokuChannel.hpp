#ifndef ROKUCHANNEL_H
#define ROKUCHANNEL_H

#include <map>
#include <string>

namespace Roku
{
  class RokuChannel
  {
   public:
    RokuChannel(std::string name);
    RokuChannel(std::string name,
                std::map<std::string, std::string> attributes);
    RokuChannel(std::string name, std::map<std::string, std::string> attributes,
                std::string iconBytes);
    RokuChannel();
    std::string getName() const;
    std::string getAttribute(std::string attributeName) const;
    std::string getIconBytes() const;

   private:
    std::string name;
    std::map<std::string, std::string> attributes;
    std::string iconBytes;
  };
}  // namespace Roku
#endif  // ROKUCHANNEL_H
