#ifndef ROKU_H
#define ROKU_H

#include <future>
#include <string>
#include <vector>

#include "RokuChannel.hpp"
#include "cpr/response.h"

namespace Roku
{
  class Key
  {
   public:
    enum Value : uint8_t
    {
      Up,
      Down,
      Left,
      Right,
      Backspace,
      Select,
      Back,
      Space,
      PlayPause,
      Reverse,
      Forward,
      Home
    };

    Key() = default;
    constexpr Key(Value v) : value(v){};
    constexpr bool operator==(Key o) const { return value == o.value; }
    constexpr bool operator!=(Key o) const { return value != o.value; }

    std::string toString()
    {
      switch (this->value)
      {
        case Up:
          return "Up";
        case Down:
          return "Down";
        case Left:
          return "Left";
        case Right:
          return "Right";
        case Backspace:
          return "Backspace";
        case Back:
          return "Back";
        case Select:
          return "Select";
        case PlayPause:
          return "Play";
        case Forward:
          return "Forward";
        case Reverse:
          return "Reverse";
        case Space:
          return "Space";
        case Home:
          return "Home";
      }
    }

   private:
    Value value;
  };

  class Roku
  {
   public:
    Roku(std::string ip, std::string port);
    std::vector<RokuChannel> getChannels() const;
    bool hasChannel(std::string name) const;
    std::string getIp() const;
    std::string getPort() const;
    bool launch(std::string name) const;
    bool hasChannelList() const;
    std::future<cpr::Response> sendKey(Key key) const;
    std::future<cpr::Response> sendChar(std::string c) const;

   private:
    std::string ip;
    std::string port;
    bool haveChannelList = false;
    std::vector<RokuChannel> channels;
    std::string baseUrl;

    void downloadChannels();
    std::string queryUrl(std::string q = "") const;
    std::string keypressUrl(std::string k) const;
    std::string launchUrl(std::string c) const;
  };
}  // namespace Roku
#endif  // ROKU_H
